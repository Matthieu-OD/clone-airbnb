module.exports = {
  mode: 'jit',
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './features/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['"Inter", sans-serif'],
    },
    extend: {
      colors: {
        active: '#ff385c',
        secondary: '#b0b0b0',
      },
      backgroundImage: () => ({
        'destination-mobile': 'url("/explore/bg-image-mobile.webp")',
      }),
      height: {
        30.5: '30.5rem',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
