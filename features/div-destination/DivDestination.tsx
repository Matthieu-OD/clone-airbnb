import React from 'react'

import 'tailwindcss/tailwind.css'

import InputDestination from '../input-destination'
import CenterInfo from './CenterInfo'

/**
 * @return Div with the informations for the user to choose a destination
 */
export default function DivDestination() {
  return (
    <div
      className={`bg-center bg-contain bg-destination-mobile h-30.5
      position-relative flex flex-col`}
    >
      <div className="flex-grow-0">
        <InputDestination />
      </div>
      <div className="flex items-center justify-center flex-grow px-6">
        <CenterInfo />
      </div>
    </div>
  )
}
