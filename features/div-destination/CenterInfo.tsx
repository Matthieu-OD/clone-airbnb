import React from 'react'

/**
 * @return CenterInfo component for div-destination
 */
export default function CenterInfo() {
  return (
    <div className="flex flex-col items-center text-black">
      <p className="text-lg font-medium">Not sure where to go?</p>
      <p className="text-lg font-medium">Perfect.</p>
      <button className="px-8 py-3 mt-3 bg-white shadow-xl rounded-3xl">
        <p
          className={`text-transparent bg-gradient-to-r from-purple-600
          to-pink-600 bg-clip-text font-bold text-lg`}
        >
          I&apos;m flexible
        </p>
      </button>
    </div>
  )
}
