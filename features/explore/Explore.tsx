import React from 'react'

import DivCovidInfo from '../div-covid-info'
import DivDestination from '../div-destination'
import ExploreNearby from '../explore-nearby'

/**
 * @return Page Explore
 */
export default function Explore() {
  return (
    <>
      <DivCovidInfo />
      <DivDestination />
      <ExploreNearby />
    </>
  )
}
