import React from 'react'

import 'tailwindcss/tailwind.css'

import { SearchIcon } from '@heroicons/react/outline'

/**
 * @return InputDestination component
 */
export default function InputDestination() {
  return (
    <div className="sticky flex items-center justify-center bg-transparent">
      <div
        className={`flex items-center justify-center py-3.5 px-20 bg-white
        rounded-3xl mt-4`}
      >
        <SearchIcon className="w-5 h-5 text-active" />
        <input
          className="ml-3 text-sm font-medium placeholder-black w-36 focus:outline-none"
          type="text"
          placeholder="Where are you goint?"
        />
      </div>
    </div>
  )
}
