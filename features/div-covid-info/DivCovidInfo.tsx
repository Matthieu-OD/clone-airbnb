import React from 'react'

import 'tailwindcss/tailwind.css'

/**
 * Div for COVID informations
 * @return div for covid informations
 */
export default function DivCovidInfo() {
  return (
    <div className="flex items-center justify-center h-12 bg-gray-200">
      <a target="_blank" href="/" className="text-sm underline">
        Get the latest on our COVID-19 response
      </a>
    </div>
  )
}
