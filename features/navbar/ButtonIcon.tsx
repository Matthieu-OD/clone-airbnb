import React from 'react'

import PropTypes from 'prop-types'
import { SearchIcon, HeartIcon, UserIcon } from '@heroicons/react/outline'
import 'tailwindcss/tailwind.css'

type ButtonIconProps = {
  labelValue: string
  iconTag: string
  active: boolean
}

/**
 * Component for Footer Button Icon
 * @param labelValue the label of the current value
 * @param iconTag the name of the current icon
 * @param active true if we are on the active button
 * @return Button Icon Component
 */
export default function ButtonIcon({
  labelValue,
  iconTag,
  active,
}: ButtonIconProps): JSX.Element {
  let icon: JSX.Element | undefined = undefined

  switch (iconTag) {
    case 'SearchIcon': {
      icon = <SearchIcon className={`w-5 h-5 ${active ? 'text-active' : ''}`} />
      break
    }
    case 'HeartIcon': {
      icon = <HeartIcon className={`w-5 h-5 ${active ? 'text-active' : ''}`} />
      break
    }
    case 'UserIcon': {
      icon = <UserIcon className={`w-5 h-5 ${active ? 'text-active' : ''}`} />
      break
    }
    default:
      break
  }

  return (
    <div className="flex flex-col items-center justify-center w-28 text-secondary">
      {icon}
      <p className={`text-sm ${active ? 'text-black' : ''}`}>{labelValue}</p>
    </div>
  )
}

ButtonIcon.defaultProps = {
  active: false,
}

ButtonIcon.propTypes = {
  labelValue: PropTypes.string.isRequired,
  iconTag: PropTypes.string.isRequired,
  active: PropTypes.bool,
}
