import React, { useState } from 'react'

import 'tailwindcss/tailwind.css'

import ButtonIcon from './ButtonIcon'

const values: Array<Array<string>> = [
  ['SearchIcon', 'Explore'],
  ['HeartIcon', 'Whishlists'],
  ['UserIcon', 'Log in'],
]

/**
 * RFC for the Navbar
 * @return The Navbar Component
 */
export default function Navbar() {
  const [currentId, setCurrentId] = useState<number>(0)

  const handleClick = (index: number) => {
    setCurrentId(index)
  }

  return (
    <div
      className={`fixed bottom-0 left-0 right-0 flex justify-center h-16
      border-t border-secondary border-solid bg-white`}
    >
      {values.map(([iconTag, labelValue], index) =>
        index === currentId ? (
          <ButtonIcon
            key={index}
            active
            labelValue={labelValue}
            iconTag={iconTag}
          />
        ) : (
          <button key={index} onClick={() => handleClick(index)}>
            <ButtonIcon labelValue={labelValue} iconTag={iconTag} />
          </button>
        )
      )}
    </div>
  )
}
