import React from 'react'

import 'tailwindcss/tailwind.css'

import CardNearbyLocation from './CardNearbyLocation'

type Card = {
  cardName: string
  cardTime: string
  cardPath: string
}

const nearbyDestinations: Array<Card> = [
  {
    cardName: 'Paris',
    cardTime: '2 hour drive',
    cardPath: '/explore-nearby/img1.webp',
  },
  {
    cardName: 'Saint-Malo',
    cardTime: '5 hour drive',
    cardPath: '/explore-nearby/img2.webp',
  },
  {
    cardName: 'Nantes',
    cardTime: '5.5 hour drive',
    cardPath: '/explore-nearby/img3.webp',
  },
  {
    cardName: 'Lille',
    cardTime: '2 hour drive',
    cardPath: '/explore-nearby/img4.webp',
  },
  {
    cardName: 'Deauville',
    cardTime: '2.5 hour drive',
    cardPath: '/explore-nearby/img5.webp',
  },
  {
    cardName: 'Rennes',
    cardTime: '5 hour drive',
    cardPath: '/explore-nearby/img6.webp',
  },
  {
    cardName: 'Dijon',
    cardTime: '5 hour drive',
    cardPath: '/explore-nearby/img7.webp',
  },
  {
    cardName: 'Vannes',
    cardTime: '6 hour drive',
    cardPath: '/explore-nearby/img8.webp',
  },
]

/**
 * @return ExploreNearby component
 */
export default function ExploreNearby() {
  return (
    <div className="px-6 pb-24">
      <div className="pb-2 bg-white pt-9">
        <p className="text-2xl font-bold">Explore nearby</p>
      </div>
      <div className="flex flex-col flex-wrap h-48 overflow-scroll">
        {nearbyDestinations.map(({ cardName, cardTime, cardPath }) => (
          <CardNearbyLocation
            key={cardName}
            cardName={cardName}
            cardTime={cardTime}
            cardPath={cardPath}
          />
        ))}
      </div>
    </div>
  )
}
