import React from 'react'

import PropTypes from 'prop-types'
import 'tailwindcss/tailwind.css'

type CardNearbyLocationProps = {
  cardName: string
  cardTime: string
  cardPath: string
}

/**
 * Might use this component elsewhere on the page
 * @return Card component for Explore Nearby
 */
export default function CardNearbyLocation({
  cardName,
  cardTime,
  cardPath,
}: CardNearbyLocationProps): JSX.Element {
  return (
    <div className="flex items-center w-auto mt-4 mr-16">
      <div className="mr-3">
        <img
          className="rounded-xl"
          src={cardPath}
          alt="Card picture"
          width="72"
          height="72"
        />
      </div>
      <div>
        <p className="font-semibold">{cardName}</p>
        <p>{cardTime}</p>
      </div>
    </div>
  )
}

CardNearbyLocation.propTypes = {
  cardName: PropTypes.string.isRequired,
  cardTime: PropTypes.string.isRequired,
  cardPath: PropTypes.string.isRequired,
}
