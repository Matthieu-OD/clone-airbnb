import React from 'react'

import PropTypes from 'prop-types'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import 'tailwindcss/tailwind.css'

import Navbar from '../features/navbar'

/**
 * Wrapper for all component.
 * @param Component children component
 * @param pageProps children props
 * @return Component Wrapped in MyApp component
 */
export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Airbnb Clone</title>
        <link rel="icon" href="/icon-airbnb.svg" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <body>
        <Component {...pageProps} />
        <Navbar />
      </body>
    </>
  )
}

MyApp.propTypes = {
  Component: PropTypes.func,
  pageProps: PropTypes.objectOf(PropTypes.any),
}
