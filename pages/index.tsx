import React from 'react'

import Explore from '../features/explore'

/**
 * Home Page of the application
 * @return RFC for the Home page
 */
export default function Home() {
  return (
    <>
      <Explore />
    </>
  )
}
